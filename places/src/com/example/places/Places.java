package com.example.places;

import android.graphics.Bitmap;

public class Places {
	String name, address, place_id, lat, lng, ratings, photoreference,
	opennow = "";
	Bitmap image;

	public Places(String nm, String ad, String pid, String lat, String lng,
			String rat, String pref, String opennow) {

		this.name = nm;
		this.address = ad;
		this.place_id = pid;
		this.lat = lat;
		this.lng = lng;

		this.ratings = rat;
		this.photoreference = pref;
		this.opennow = opennow;

	}

	public void setImage(Bitmap bmp) {
		this.image = bmp;
	}
}
