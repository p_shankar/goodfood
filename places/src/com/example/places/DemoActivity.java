package com.example.places;


import android.app.Activity;
import android.os.Bundle;
import android.widget.HorizontalScrollView;

public class DemoActivity extends Activity {

	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setRequestedOrientation(1);
		requestWindowFeature(1);
		getWindow().setFlags(1024, 1024);
		setContentView(R.layout.activity_demo);
		autoSmoothScroll();
	}

	public void autoSmoothScroll() {
		final HorizontalScrollView localHorizontalScrollView = (HorizontalScrollView) findViewById(R.id.hv_demo);
		localHorizontalScrollView.postDelayed(new Runnable() {
			public void run() {
				localHorizontalScrollView.smoothScrollBy(50, 0);
			}
		}, 10L);
	}
}
