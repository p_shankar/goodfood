package com.example.places;



import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class HelpActivity extends Activity {
	ListView helplv;
	HelpAdapter ha;
	Dialog dialog;
	Button db1;
	EditText guys;
	AdView adView;
View rootView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);

		ha = new HelpAdapter(this);

		helplv = (ListView) findViewById(R.id.listView1);
		helplv.setAdapter(ha);

		helplv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> paramAnonymousAdapterView,
					View paramAnonymousView, final int paramAnonymousInt,
					long paramAnonymousLong) {

				switch (paramAnonymousInt) {
				case 0: // *********************************DEMO************************************
					Intent set = new Intent(getApplicationContext(),
							DemoActivity.class);
					set.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					getApplicationContext().startActivity(set);
					HelpActivity.this.overridePendingTransition(
							R.anim.push_down_in, R.anim.push_down_out);

					break;
				case 1: // *********************************EMAIL************************************
					dialog = new Dialog(HelpActivity.this);
					dialog.setContentView(R.layout.help_email);
					dialog.setTitle("Help");

					db1 = (Button) dialog.findViewById(R.id.bt_help_fb_send);

					// if button is clicked, close the custom dialog
					db1.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							guys = (EditText) dialog
									.findViewById(R.id.et_help_fb_msg);
							if (guys.getText().toString() != null) {

								Intent mail = new Intent(Intent.ACTION_SENDTO,
										Uri.fromParts("mailto",
												"omkar.9194@gmail.com", null));
								mail.putExtra(Intent.EXTRA_SUBJECT,
										"Great Place!");
								mail.putExtra(Intent.EXTRA_TEXT, guys.getText()
										.toString());

								try {
									startActivity(Intent.createChooser(mail,
											"Send mail..."));
								} catch (android.content.ActivityNotFoundException ex) {
									Toast.makeText(
											HelpActivity.this,
											"There are no email clients installed.",
											Toast.LENGTH_SHORT).show();
								}

								dialog.dismiss();
							}

						}
					}

							);

					dialog.show();
					break;
				case 2:
					break;
				case 3: // *********************************SETTINGS/FILTERS************************************
					Intent set1 = new Intent(getApplicationContext(),
							Filters.class);
					set1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					getApplicationContext().startActivity(set1);
					HelpActivity.this.overridePendingTransition(
							R.anim.push_down_in, R.anim.push_down_out);

				}
			}
		});
		rootView=getWindow().getDecorView().findViewById(android.R.id.content);
//		adView=(AdView)rootView.findViewById(R.id.MyAdView);
//		 AdRequest adRequest=new AdRequest.Builder().build();
//		 
////			AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
////					 .addTestDevice("F0E3A57BED5A38D34F93EB7CD6873BC9")
////						.build();
//			 adView.loadAd(adRequest);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.help, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
