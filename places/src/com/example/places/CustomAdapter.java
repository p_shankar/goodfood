package com.example.places;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter {
	Context context;
	List<Places> places;
	String arr[];

	CustomAdapter(Context context, List<Places> places) {
		this.context = context;
		this.places = places;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return places.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub

		return places.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	private class ViewHolder {
		ImageView restaurant_pic;
		TextView restaurant_name;

		TextView address;
		TextView opennow;
		RatingBar rbar;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item, null);
			holder = new ViewHolder();

			holder.restaurant_pic = (ImageView) convertView
					.findViewById(R.id.restaurant_pic);
			holder.restaurant_name = (TextView) convertView
					.findViewById(R.id.restaurant_name);
			holder.restaurant_pic = (ImageView) convertView
					.findViewById(R.id.restaurant_pic);
			holder.address = (TextView) convertView
					.findViewById(R.id.textView1);
			holder.opennow = (TextView) convertView.findViewById(R.id.opennow);
			holder.rbar = (RatingBar) convertView.findViewById(R.id.ratingBar1);
			holder.address.setMovementMethod(new ScrollingMovementMethod());

			// holder.restaurant_pic.setImageResource(row_pos.getProfile_pic_id());

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Places plcs = places.get(position);

		holder.restaurant_name.setText(plcs.name);
		holder.address.setText(plcs.address);
		holder.opennow.setText(plcs.opennow);
		holder.restaurant_pic.setImageBitmap(plcs.image);

		holder.rbar.setMax(5);
		holder.rbar.setRating(Float.parseFloat(plcs.ratings));
		holder.rbar.setStepSize(0.5f);

		return convertView;
	}

}
